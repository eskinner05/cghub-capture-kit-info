This is the repository where CGHub and GSCs submitting to CGHub collaboratively maintain capture
kit information for whole exome sequence data hosted at CGHub. This README is meant for internal
use at CGHub. The GSCs and the public should refer to the wiki_ instead.

.. _wiki: https://bitbucket.org/cghub/cghub-capture-kit-info/wiki/Home


Prerequisites
=============

* git >= 1.7.0
* Python 2.7.x
* cghub-metadata-tools_

.. _cghub-metadata-tools: https://bitbucket.org/cghub/cghub-metadata-tools


Preparing the repository for import
===================================

Clone the repository, check the validity of relative file ``.bed`` paths in the ``.tsv``, and cache
``.bed`` files referred to by external URLs in the ``.tsv``.

::

   git clone git@bitbucket.org:cghub/cghub-capture-kit-info.git
   cd cghub-capture-kit-info
   ./manage.py --check-files
   ./manage.py --cache-urls

If both succeed, commit any newly cached files::

   git add cache/*

Otherwise fix the problem, rinse and repeat. The fix is likely going to be a modification to the
``.tsv`` or the renaming or addition of a ``.bed`` file. Commit those changes in a separate commit
before committing cached files.


Importing the repository into CGHub metadata
============================================

1. Change into the repository if you haven't done so already::

      cd cghub-capture-kit-info

2. Run

   ::

      cgmetamigrate --verbose \
                    CaptureKitImport \
                    --repository . \
                    --validate-modified-only \
                    webservice \
                    --dump dump \
                    --dry-run

   The first positional parameter selects the migration to be performed.

   The ``--repository`` flag points at the capture kit repository that we prepared in the previous
   section.

   The second positional parameter selects the migrator to be used. Here we use the webservice
   migrator that reads metadata from the database--not SOLR, because the database is the
   authoritative copy--and writes the migrated metadata back using the internal SPM webservice
   endpoint for metadata updates. 

   The ``--dump`` flag dumps the metadata of each migrated analysis before and after the migration
   is applied.

   The ``--dry-run`` flag goes through the motions but prevents changed metadata from being written
   back.

   The ``--validate-modified-only``flag skips the validation of unmodified submissions. We
   currently have a few submissions in the system that fail our own validations. This flag helps
   working around these as long as they aren't touched by the migration.

   There are various flags for configuring the webservice endpoint and the database connection but
   if you run this on app01, the defaults should work just fine.

3. Diff the metadata dump and eyeball it. I typically do

   ::

      diff -rw dump/input dump/output > dump/migration.diff

   and then

   ::

      sort dump/migration.diff | uniq -c | sort -rn | less


4. Delete the ``dump`` directory

5. Run ``cgmetamigrate`` again without the ``--dry-run`` flag


Exporting from CGHub metadata into the repository
=================================================

Export is the inverse of import: it scans the CGHub metadata for any capture kit info that was
directly submitted by the center through the normal submission process and adds the corresponding
rows to the TSVs in the repository. Currently only the legacy XML format is supported by the
export migration but JSON support could easily be added. Reimporting the TSV updated by a preceding
export can be used to convert capture kit info from the legacy XML format to the current JSON
format.

The export attempts to only modify a TSV if actual rows need to be added. However, this is a best
effort only--there are various conditions under which the export may modify a TSV syntactically
without an actual change in semantics of the TSV.

1. Change into the repository if you haven't done so already::

      cd cghub-capture-kit-info

The working copy must be clean but unlike with import its ok to have unpushed commits.

2. Run

   ::

      cgmetamigrate --verbose CaptureKitExport \
                    --repository ../cghub-capture-kit-info \
                    webservice \
                    --admin-token /Users/hannes/CGADMIN.pem

   To import a single center, add a corresponding SQL clause::

      cgmetamigrate --verbose CaptureKitExport \
                    --repository ../cghub-capture-kit-info \
                    webservice \
                    --admin-token /Users/hannes/CGADMIN.pem \
                    --db-where "center_name='WUGSC'"

3. Check internal .bed file references and cache external .bed files

   ::

      ./manage.py --check-files
      ./manage.py --cache-urls

4. Commit the updated TSVs and any newly cached files

You can now run an import to replace the legacy XML capture kit information with JSON format.
