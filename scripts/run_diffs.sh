diff -rw dump/input dump/output > dump/migration.diff
sort dump/migration.diff | uniq -c | sort -rn > dump/migration.diff.sorted
./scripts/process_diff.pl dump/migration.diff > dump/migration.diff.cut
sort dump/migration.diff.cut | uniq -c | sort -rn > dump/migration.diff.cut.sorted
#should match total expect "real" changes (not just link updates)
fgrep "CAPTURE_KIT_INFO" dump/migration.diff | wc -l
